﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPrac.Security;
using PMS.BAL;
using PMS.Entities;
namespace WebPrac.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Test()
        {
            int c = 0;
            int b = 0;
            int a = 0;
            return null;
        }
        public ActionResult Admin()
        {
            
            if (SessionManager.IsValidUser)
            {
               
                if (SessionManager.User.IsAdmin)
                {
                    ViewBag.username = SessionManager.User.Name;
                    ViewBag.img = SessionManager.User.PictureName;
                    ViewBag.login = SessionManager.User.Login;
                    ViewBag.id = SessionManager.User.UserID;
                    return View();
                }
                else
                {
                    ViewBag.username = SessionManager.User.Name;
                    ViewBag.img = SessionManager.User.PictureName;
                    ViewBag.login = SessionManager.User.Login;
                    ViewBag.id = SessionManager.User.UserID;
                    return RedirectToAction("NormalUser");
                }
            }
            else
            {
                return Redirect("~/User/Login");
            }
        }
        public ActionResult NormalUser()
        {

            if (SessionManager.IsValidUser)
            {

                if (SessionManager.User.IsAdmin)
                {
                    ViewBag.username = SessionManager.User.Name;
                    ViewBag.img = SessionManager.User.PictureName;
                    ViewBag.login = SessionManager.User.Login;

                    return RedirectToAction("Admin");
                }
                else
                {
                   
                    ViewBag.username = SessionManager.User.Name;
                    ViewBag.img = SessionManager.User.PictureName;
                    ViewBag.login = SessionManager.User.Login;
                    return View();
                }
            }
            else
            {
                return Redirect("~/User/Login");
            }
        }
    }
}