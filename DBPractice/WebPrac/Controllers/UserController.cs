﻿using PMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPrac.Security;
using PMS.BAL;
namespace WebPrac.Controllers
{
    public class UserController : Controller
    {
        private static string code;
        Random r = new Random();

        public ActionResult Code()
        {
            if (Session["resetPressed"] != null)
            {
                ViewBag.MSG = "Enter the code here";
                ViewBag.login = Session["login"];
                return View();
            }
            else
                return RedirectToAction("Login");
        }     
        [HttpPost]
        [ActionName("Code")]
        public ActionResult Code2()
        {
            if (Request["codeBtn"] != null)
            {
                string abc = Request["code"];
                if (abc == code.ToString())
                {
                    ViewBag.login = Request["login"];
                    return View("Reset");
                }
                else
                    ViewBag.MSG = "You've entered wrong code";
            }

            return View();
        }
        public ActionResult Reset()
        {
            if (Session["resetPressed"] != null)
            {
               
                ViewBag.login = Session["login"];
                Session["resetPressed"] = null;
                return View();
            }
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        [ActionName("Reset")]
        public ActionResult Reset2()
        {
            if (Request["change"] != null)
            {

                string login = Request["login"];
                UserDTO u = UserBO.GetUserByLogin(login);
                u.Login = login;
                u.Password = Request["password"];
                if (UserBO.UpdatePassword(u)!=0)
                {
                    ViewBag.username = u.Name;
                    ViewBag.img = u.PictureName;
                    ViewBag.login = u.Login;
                    SessionManager.User = u;
                    if (u.IsAdmin == false)
                    {
                        return Redirect("~/Home/NormalUser");
                    }
                    else
                    {
                        return Redirect("~/Home/Admin");
                    }
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult ChangePass()
        {
            if(Request["back"]!=null)
            {
                ViewBag.back = Request["back"];
            }
            return View();
        }
        [HttpPost]
        [ActionName("ChangePass")]
        public ActionResult ChangePass2()
        {
            if(Request["changepass"]!=null)
            {
                UserDTO u = UserBO.GetUserByLogin(SessionManager.User.Login);
                string oldpass = Request["oldpass"];
                string newpass = Request["newpass"];
                if(oldpass==u.Password)
                {
                    u.Password = newpass;
                    UserBO.UpdatePassword(u);
                    if (u.IsAdmin)
                      return   Redirect("~/Home/Admin");
                    else
                      return   Redirect("~/Home/NormalUser");

                }
                else
                {
                    ViewBag.error = "Enter correct old password please!";
                    return View();
                }
                
            }
            return View();
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(String login, String password)
        {
            if (Request["btnLogin"] == "Reset Password")
            {
                if (Request["login"] != null)
                {
                    Session["resetPressed"] = "true";
                    string Email = Request["email"];
                    string l = Request["login"];
                    string subject = "ResetPassword";
                   
                    code = r.Next(1000, 10000).ToString(); // 4 digit password
                    string body = code;
                    if (UserBO.sendEmail(Email, subject, body))
                    {
                        ViewBag.login = l;
                        Session["login"] = l;
                        return RedirectToAction("Code");
                    }
                }
                else
                {
                    ViewBag.error = "Enter login for which you want to reset password!";
                    return View();
                }
            }
            else if (Request["btnLogin"] == "Login")
            {
                if(Request["login"]==null || Request["password"]==null)
                {
                    ViewBag.error = "Enter all fields plz";
                    return View();
                }
                var obj = PMS.BAL.UserBO.ValidateUser(login, password);
                if (obj != null)
                {

                    if (obj.IsAdmin == true)
                    {
                        ViewBag.login = obj.Login;
                        SessionManager.User = obj;
                        return Redirect("~/Home/Admin");
                    }

                    else
                    {
                        ViewBag.login = obj.Login;
                        SessionManager.User = obj;
                        return Redirect("~/Home/NormalUser");
                    }

                }

                ViewBag.MSG = "Invalid Login/Password";
                ViewBag.Login = login;

               
            }
            return View();
        }
        [HttpGet]
        public ActionResult NewUser()
        {

            return View();
        }

        [HttpGet]
        public ActionResult Edit()
        {
            if (Request["edit"] != null)
            {
                ViewBag.id = Request["id"];
                ViewBag.img = Request["image"];
                ViewBag.name = Request["name"];
                ViewBag.login = Request["login"];

            }
            else
                ViewBag.error = "Try Again";
            return View();

        }
        [HttpPost]
        [ActionName("Edit")]
        public ActionResult Edit2()
        {
            if (Request["edit"] == "Save Changes")
            {

                // validation ends
                UserDTO l = UserBO.GetUserByLogin(Request["login"]);
                UserDTO u = new UserDTO();
                if (l != null)
                {
                    u.IsAdmin = l.IsAdmin;
                    u.PictureName = l.PictureName;
                    u.UserID = l.UserID;
                }

                u.Login = Request["login"];
                var file = Request.Files["filename"];

                if (file.FileName != "")
                {
                    u.PictureName = file.FileName;
                    var rootPath = Server.MapPath("~/userPics");
                    var fileSavePath = System.IO.Path.Combine(rootPath, u.PictureName);
                    file.SaveAs(fileSavePath);
                }

                u.Name = Request["name"];
                u.Password = Request["password"];
                int choice = UserBO.Save(u);
                if (choice != 0)
                {
                    if (WebPrac.Security.SessionManager.User.IsAdmin)
                    {
                        ViewBag.username = u.Name;
                        ViewBag.img = u.PictureName;
                        ViewBag.login = u.Login;
                        SessionManager.User = u;
                        return Redirect("~/Home/Admin");
                    }
                    else
                    {
                      
                        ViewBag.username = u.Name;
                        ViewBag.img = u.PictureName;
                        ViewBag.login = u.Login;
                        SessionManager.User = u;
                        return Redirect("~/Home/NormalUser");
                    }

                }
                else
                {
                    ViewBag.error = "Try Again!";
                   
                }              
            }
            return View();
        }
        [HttpPost]
        [ActionName("NewUser")]
        public ActionResult NewUser2()
        {
            // validation
            if (Request["create"] == "Sign Up")
            {

                if (Request["name"] == null || Request["password"] == null || Request["login"] == null)
                {
                   
                    ViewBag.error = "Enter all fields please!";
                    return View();
                }
               
            
                // validation ends
                UserDTO u = new UserDTO();
                u.Login= Request["login"];
                var file = Request.Files["filename"];

                if (file.FileName != "")
                {
                    u.PictureName = file.FileName;
                    var rootPath = Server.MapPath("~/userPics");
                    var fileSavePath = System.IO.Path.Combine(rootPath, u.PictureName);
                    file.SaveAs(fileSavePath);
                }
                else
                {
                   
                    ViewBag.error = "Upload some picture too!";
                    return View();
                }
                u.Name = Request["name"];
                u.Password = Request["password"];

                
                UserDTO a = UserBO.GetUserByLogin(u.Login);
                if(a!=null)
                {
                 
                    ViewBag.error = "User Already Exists";
                    return View();
                }
                int choice = UserBO.Save(u);
                if (choice == 1)
                {

                    ViewBag.username = u.Name;
                    ViewBag.img = u.PictureName;
                    ViewBag.login = u.Login;
                    SessionManager.User = u;
                    return Redirect("~/Home/NormalUser");
                }
            }
            return View();  
         
        }

    [HttpPost]
        public ActionResult Save(UserDTO dto)
        {
           
            return View();
        }

        [HttpGet]
        public ActionResult Logout()
        {
            SessionManager.ClearSession();
            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Login2()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ValidateUser(String login, String password)
        {

            Object data = null;

            try
            {
                var url = "";
                var flag = false;

                var obj = PMS.BAL.UserBO.ValidateUser(login, password);
                if (obj != null)
                {
                    flag = true;
                    SessionManager.User = obj;

                    if (obj.IsAdmin == true)
                        url = Url.Content("~/Home/Admin");
                    else
                        url = Url.Content("~/Home/NormalUser");
                }

                data = new
                {
                    valid = flag,
                    urlToRedirect = url
                };
            }
            catch (Exception)
            {
                data = new
                {
                    valid = false,
                    urlToRedirect = ""
                };
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
	}
}