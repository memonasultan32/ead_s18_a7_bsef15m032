﻿using PMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPrac.Security;

namespace WebPrac.Controllers
{
    public class ProductController : Controller
    {
        [HttpGet]
        public JsonResult getUserName()
        {

            Object data = null;

            try
            {
                var url = "";
                string n = "";
                int id = Convert.ToInt32(Request["id"]);
                var obj = PMS.BAL.UserBO.GetUserById(id);
                if (obj != null)
                {
                    url = Url.Content("~/Product/ShowAll");
                    n = obj.Name;

                }

                data = new
                {
                    name = n
                };
            }
            catch (Exception)
            {
                data = new
                {
                    name = ""

                };
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        private ActionResult GetUrlToRedirect()
        {
            if (SessionManager.IsValidUser)
            {
                //if (SessionManager.User.IsAdmin == false)
                //{
                //    TempData["Message"] = "Unauthorized Access";
                //    return Redirect("~/Home/NormalUser");
                //}
            }
            else
            {
                TempData["Message"] = "Unauthorized Access";
                return Redirect("~/User/Login");
            }

            return null;
        }
        public ActionResult ShowAll()
        {
            if (SessionManager.IsValidUser == false)
            {
                return Redirect("~/User/Login");
            }

            var products = PMS.BAL.ProductBO.GetAllProducts(true);

            return View(products);
        }

        public ActionResult New()
        {
            var redVal = GetUrlToRedirect();
            if (redVal == null)
            {
                TempData["required"] = "required";
                var dto = new ProductDTO();
                redVal = View(dto);
            }

            return redVal;
        }

        public ActionResult Edit(int id)
        {
            if (WebPrac.Security.SessionManager.User != null)
            {
                var redVal = GetUrlToRedirect();
                ProductDTO p = PMS.BAL.ProductBO.GetProductById(id);

                if (p.CreatedBy == SessionManager.User.UserID || SessionManager.User.IsAdmin == true)
                {
                    var prod = PMS.BAL.ProductBO.GetProductById(id);
                    redVal = View("New", prod);
                }
                else
                {
                    TempData["Msg"] = "Unauthorized access";
                    TempData["required"] = "";
                    return RedirectToAction("ShowAll");
                }

                return redVal;
            }
            else
                return Redirect("~/User/Login");

        }
        public ActionResult SaveComment(string txtComment, int prodID)
        {
            if (WebPrac.Security.SessionManager.User != null)
            {
                if (txtComment != "")
                {
                    CommentDTO cm = new CommentDTO();
                    cm.UserID = SessionManager.User.UserID;
                    cm.UserName = SessionManager.User.Name;
                    cm.PictureName = SessionManager.User.PictureName;
                    cm.CommentOn = DateTime.Now;
                    cm.CommentText = txtComment;
                    cm.ProductID = prodID;
                    if (PMS.BAL.CommentBO.Save(cm) != 0)
                    {
                        return RedirectToAction("ShowAll");
                    }
                }
                else
                {
                    TempData["Msg"] = "Please enter some text";
                    

                }
                return RedirectToAction("ShowAll");

            }
            else
                Redirect("~/User/Login");
            return RedirectToAction("ShowAll");
        }
        public ActionResult Delete(int id)
        {
            if (WebPrac.Security.SessionManager.User != null)
            {
                ProductDTO p = PMS.BAL.ProductBO.GetProductById(id);
                if (p.CreatedBy == SessionManager.User.UserID || SessionManager.User.IsAdmin == true)
                {

                    PMS.BAL.ProductBO.DeleteProduct(id);
                    TempData["Msg"] = "Record is deleted!";
                    return RedirectToAction("ShowAll");
                }
                else
                {
                    TempData["Msg"] = "Unauthorized access";
                    return RedirectToAction("ShowAll");
                }
            }
            else
                return Redirect("~/User/Login");

        }
        [HttpPost]
        public ActionResult Save(ProductDTO dto)
        {

            if (dto.Name == "" || dto.Price==0 || dto.Price<0)
            {
                TempData["Msg"] = "Fill all the fields";
                return View("New",new ProductDTO());
            }


            if (SessionManager.IsValidUser)
            {

                var uniqueName = "";

                if (Request.Files["Image"] != null)
                {
                    var file = Request.Files["Image"];
                    if (file.FileName != "")
                    {
                        var ext = System.IO.Path.GetExtension(file.FileName);

                        //Generate a unique name using Guid
                        uniqueName = Guid.NewGuid().ToString() + ext;

                        //Get physical path of our folder where we want to save images
                        var rootPath = Server.MapPath("~/UploadedFiles");

                        var fileSavePath = System.IO.Path.Combine(rootPath, uniqueName);

                        // Save the uploaded file to "UploadedFiles" folder
                        file.SaveAs(fileSavePath);

                        dto.PictureName = uniqueName;
                    }
                }



                if (dto.ProductID > 0)
                {
                    dto.ModifiedOn = DateTime.Now;
                    // dto.ModifiedBy = 1;   // error fixed
                    dto.ModifiedBy = SessionManager.User.UserID;
                }
                else
                {
                    dto.CreatedOn = DateTime.Now;
                    // dto.CreatedBy = 1;   // error fixed
                    dto.CreatedBy = SessionManager.User.UserID;
                }

                PMS.BAL.ProductBO.Save(dto);

                TempData["Msg"] = "Record is saved!";

                return RedirectToAction("ShowAll");
            }
            else
            {
                return Redirect("~/User/Login");
            }


          
        }
        public ActionResult ShowProfile(int uid)
        {
            if (WebPrac.Security.SessionManager.User != null)
            {
                UserDTO udto = PMS.BAL.UserBO.GetUserById(uid);
                if (udto != null)
                {
                    return View("ShowProfile", udto);
                }
                return RedirectToAction("ShowAll");
            }
            else
                return Redirect("~/User/Login");

          
        }

    }
}